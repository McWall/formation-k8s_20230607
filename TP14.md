# TP 14 - Les ressources

```bash
mkdir ../TP14 && cd ../TP14
kubectl get nodes
kubectl describe node minikube | less
kubectl describe pod my-db | less
kubectl describe pod my-db | grep QoS # BestEffort
# Niveaux de QoS :
#	1. BestEffort (ni requests ni limits deefinies => le moins bon)
# 	2. Burstable (requests different de limits)
#	3.  (requests = limits)
kubectl -n ingress-nginx get pods
kubectl -n ingress-nginx describe pod ingress-nginx-controller-6cc5ccb977-v7kwb | grep QoS # Burstable
kubectl delete all --all
kubectl run my-pod --image=samiche92/my-node-server:1.0.0 --port=8080 -o yaml --dry-run=client > my-pod.yaml
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    ports:
    - containerPort: 8080
    resources:
      requests:
        memory: "100Mi"
        cpu: "3"
```

```bash
kubectl apply -f my-pod.yaml
kubectl describe node minikube | less
cp my-pod.yaml my-pod-2.yaml 
vim my-pod-2.yaml
# name: my-pod => name: my-pod-2
kubectl get pods -w
# Dans un second terminal
kubectl apply -f my-pod-2.yaml
kubectl describe pod my-pod-2

vim my-lr.yaml
```

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: my-lr
spec:
  limits:
  - defaultRequest: # Default requests
      cpu: "1"
    default: # Default limits
      cpu: "2"
    max:
      cpu: "2"
    min:
      cpu: "500m"
    type: Container
```

```bash
kubectl apply -f my-lr.yaml
kubectl delete pod --all

vim my-rq.yaml
```

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: my-rq
spec:
  hard:
    cpu: 3
    memory: 2Gi
    pods: 5
```

```bash
```
