# TP 13 - Jobs, CronJobs

```bash
mkdir ../TP13 && cd ../TP13
kubectl create job my-job --image=busybox:glibc -o yaml --dry-run=client > my-job.yaml
vim my-job.yaml
```

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: my-job
spec:
  template:
    metadata:
    spec:
      containers:
      - image: busybox:glibc
        name: my-job
        command: [ "sh", "-c", "x=1; while [ $x -le 5 ]; do echo $x; x=$(( $x + 1 )); sleep 5s; done" ]
      restartPolicy: Never
```

```bash
kubectl apply -f my-job.yaml
kubectl logs -f my-job-6vz6r
kubectl get pods
kubectl get jobs

kubectl create cronjob my-cj --schedule='* * * * *' --image=busybox:glibc -o yaml --dry-run=client > my-cj.yaml
vim my-cj.yaml
```

```yaml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: my-cj
spec:
  jobTemplate:
    metadata:
    spec:
      template:
        metadata:
        spec:
          containers:
          - image: busybox:glibc
            name: my-cj
            command: [ "sh", "-c", "x=1; while [ $x -le 5 ]; do echo $x; x=$(( $x + 1 )); sleep 5s; done" ]
          restartPolicy: OnFailure
  schedule: '* * * * *'
```

```bash
while true; do date; sleep 1s; done
# Dans un second terminal
kubectl get pods -w
# Dans un 3e terminal
kubectl apply -f my-cj.yaml
kubectl get cronjobs
kubectl get jobs -w
```
