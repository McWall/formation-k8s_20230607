# TP 9 - Liveness Probe, Readiness Probe

```bash
mkdir ../TP09 && cd ../TP09
cp ../TP08/my-pod.yaml .
vim my-pod.yaml
```

```yaml
[...]
    command:
    - sh
    - -c
    - while true; do date; sleep 1s; done
    livenessProbe:
      exec:
        command:
          - cat
          - /app.js
```

```bash
kubectl delete all --all
kubectl get events -w
# Dans un second terminal
kubectl get pods -w
# Dans un 3e terminal
kubectl apply -f my-pod.yaml
kubectl exec my-pod -- rm /app.js
kubectl exec my-pod -- ls /
# On observe
```

## Readiness Probe

```bash
cp my-pod.yaml my-pod-rp.yaml
kubectl delete all --all
vim my-pod-rp.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: backend
    env: dev
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-ctn
    readinessProbe:
      httpGet:
        path: /
        port: 8080
    ports:
    - containerPort: 8080
```

```bash
echo '---' >> my-pod-rp.yaml
cat my-pod-rp.yaml > /tmp/tmp.yaml && cat /tmp/tmp.yaml >> my-pod-rp.yaml
cat /tmp/tmp.yaml >> my-pod-rp.yaml
cat ../TP06/my-svc.yaml >> my-pod-rp.yaml 
vim my-pod-rp.yaml
# On change la version du 1er pod (1.0.0 => 3.0.0)
# On change aussi les noms des pods (my-pod-1, my-pod-2, my-pod-3)
kubectl get events -w
# Dans un second terminal
kubectl get pods -w
# Dans un 3e terminal
kubectl apply -f my-pod-rp.yaml 
kubectl get endpoints
kubectl get svc
while true; do curl 192.168.49.2:30755; sleep 1s; done
# Dans un 4e terminal
kubectl get pods -o wide
kubectl exec my-pod-2 -- curl <my-pod-1-@ID>:8080/repair
```
